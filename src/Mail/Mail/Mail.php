<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 4/10/14
 * Time: 6:49 PM
 */

namespace Mail\Mail;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Mime;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mail\Message;
use Zend\Mime\Part;
use Zend\Mail\Protocol;
use Zend\Mail\Protocol\Smtp\Auth\login;

class Mail implements ServiceLocatorAwareInterface {

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * To Addresses
     * @var array
     */
    protected $to;

    /**
     * From Address
     * @var string
     */
    protected $from;

    /**
     * Subject
     * @var string
     */
    protected $subject;

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return array
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param array $to
     */
    public function setTo($to)
    {
        $config = $this->getServiceLocator()->get('config');

        if( array_key_exists('override', $config['mail']))
        {
            if( $config['mail']['override']['enabled'] == true )
            {
                $to = $config['mail']['override']['address'];
            }
        }

        $this->to = $to;
    }

    /**
     * Sends the message
     * @param $key
     * @param array $data
     * @param array $attachments ['content','mime','filename']
     * @throws \Zend\View\Exception\RuntimeException
     * @throws \Zend\View\Exception\DomainException
     * @throws \Exception
     * @return boolean
     */
    public function send($key, $data = array(), $attachments=array(), $sendAt = null, $connection='connection')
    {
        if(is_null($attachments)){
            $attachments = [];
        }

        $renderer = $this->getServiceLocator()->get('ViewRenderer');
        $config = $this->getServiceLocator()->get('config');

        if( ! array_key_exists('mail', $config) )
        {
            throw new \Exception('Could not retrieve mail configuration');
        }

        if( $config['mail']['enabled'] == false )
        {
            return false;
        }

        $messageConfig = @$config['mail']['configs'][$key];

        if( ! is_array($messageConfig) )
        {
            throw new \Exception('Could not retrieve message configuration for ' . $key);
        }

        $this->prepareEssentials($messageConfig);

        if( ! array_key_exists('html_template', $messageConfig) && ! array_key_exists('text_template', $messageConfig))
        {
            throw new \Exception('No templates found for ' . $key);
        }

        $htmlContent = null;
        $textContent = null;

        $layoutOverride = false;
        if(array_key_exists('layout',$messageConfig)){
            if($messageConfig['layout'] == null){
                $layoutOverride = true;
            }
        }
        if(!$layoutOverride && is_array($config['mail']['layouts']) )  // render with layouts
        {
            // HTML
            if (array_key_exists('html_template', $messageConfig))
            {

                if (isset($config['mail']['layouts']['html']))
                {
                    $templateToRender = $config['mail']['layouts']['html'];
                    $data['innerTemplate'] = $messageConfig['html_template'];
                }
                else
                {
                    $templateToRender = $messageConfig['html_template'];
                }

                $htmlContent = $renderer->render($templateToRender, $data);
            }

            // TEXT
            if (array_key_exists('text_template', $messageConfig))
            {
                if (isset($config['mail']['layouts']['text']))
                {
                    $templateToRender = $config['mail']['layouts']['text'];
                    $data['innerTemplate'] = $messageConfig['text_template'];
                }
                else
                {
                    $templateToRender = $messageConfig['text_template'];
                }

                $textContent = $renderer->render($templateToRender, $data);
            }
        }
        else // render without layouts
        {
            if( array_key_exists('html_template', $messageConfig))
            {
                $htmlContent = $renderer->render($messageConfig['html_template'], $data);
            }

            if( array_key_exists('text_template', $messageConfig))
            {
                $textContent = $renderer->render($messageConfig['text_template'], $data);
            }
        }

        $parts = array();

        if( $htmlContent !== null )
        {
            $htmlPart = new Part($this->wrap($htmlContent));
            $htmlPart->type = Mime::TYPE_HTML;
            $parts[] = $htmlPart;
        }

        if( $textContent !== null )
        {
            $textPart = new Part($this->wrap($textContent));
            $textPart->type = Mime::TYPE_TEXT;
            $parts[] = $textPart;
        }

        if(count($attachments)>0){
            foreach($attachments as $attachment){
                $part = new Part($attachment['content']);
                $part->type = $attachment['mime'];
                $part->filename = $attachment['filename'];
                $part->disposition = Mime::DISPOSITION_ATTACHMENT;
// Setting the encoding is recommended for binary data
                $part->encoding = Mime::ENCODING_BASE64;
                array_push($parts, $part);
            }
        }

        $mimeMessage = new \Zend\Mime\Message();
        $mimeMessage->setParts($parts);

        $message = new Message();
        $message->setEncoding('UTF-8');
        $message->addTo($this->getTo());
        $fromArray = explode("|",$this->getFrom());
        if(count($fromArray) == 2){
            $message->setFrom($fromArray[0],$fromArray[1]);
        }else{
            $message->setFrom($fromArray[0]);
        }

        $message->setSubject($this->getSubject());
        $message->setBody($mimeMessage);

        if(!is_null($sendAt)) {
            $message->getHeaders()->addHeaderLine('X-MC-SendAt', $sendAt);
        }

        $transport = $this->getServiceLocator()->get('MailTransport');

        /*if($connection != 'connection') {
            $transport = new \Zend\Mail\Transport\Smtp();
            $mailoptions = new SmtpOptions();
            $mailoptions->setConnectionConfig($config['mail'][$connection]);
            $mailoptions->setFromArray($config['mail'][$connection]);
            $transport->setOptions($mailoptions);
        }*/
        
        $transport->send($message);
        return true;
    }

    /**
     * Wordwrap for multibyte content, because otherwise SMTP takes it into it's own hands
     */
    private function wrap($rendered){
        // Don't bother for short content, just skip it.
        if(mb_strlen($rendered) < 700){
            return $rendered;
        }
        return $this->utf8RegexWordwrap($rendered, 800, PHP_EOL);
    }

    /**
     * wordwrap for utf8 encoded strings using a fst regex
     *
     * @param string $str
     * @param integer $len
     * @param string $break type to insert
     * @return string
     * @author ju1ius
     * From http://php.net/manual/en/function.wordwrap.php by user
     */
    private function utf8RegexWordwrap($string, $width=75, $break="\n", $cut=false)
    {
      if($cut) {
        // Match anything 1 to $width chars long followed by whitespace or EOS,
        // otherwise match anything $width chars long
        $search = '/(.{1,'.$width.'})(?:\s|$)|(.{'.$width.'})/uS';
        $replace = '$1$2'.$break;
      } else {
        // Anchor the beginning of the pattern with a lookahead
        // to avoid crazy backtracking when words are longer than $width
        $search = '/(?=\s)(.{1,'.$width.'})(?:\s|$)/uS';
        $replace = '$1'.$break;
      }
      return preg_replace($search, $replace, $string);
    }

    /**
     * Ensures that the to and from addresses are set
     * @param $config
     * @throws \Exception
     */
    protected function prepareEssentials($config)
    {
        if( $this->to == null )
        {
            if( array_key_exists('to', $config))
            {
                $this->setTo($config['to']);
            }
            else
            {
                $defaults = $this->getDefaults();
                $this->setTo($defaults['to']);
            }
        }

        if( $this->from == null )
        {
            if( array_key_exists('from', $config))
            {
                $this->setFrom($config['from']);
            }
            else
            {
                $defaults = $this->getDefaults();
                $this->setFrom($defaults['from']);
            }
        }

        if( $this->subject == null )
        {
            if( array_key_exists('subject', $config))
            {
                $this->setSubject($config['subject']);
            }
        }
    }

    /**
     * Resets the base essential fields
     */
    public function reset()
    {
        $this->to = null;
        $this->from = null;
        $this->subject = null;
    }

    /**
     * Gets the message defaults
     * @return mixed
     * @throws \Exception
     */
    protected function getDefaults()
    {
        $config = $this->getServiceLocator()->get('config');
        $defaults = $config['mail']['configs']['defaults'];

        if( ! is_array($defaults) )
        {
            throw new \Exception('Could not retrieve message defaults!');
        }

        return $defaults;
    }
}
